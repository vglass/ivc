#include "ivcbackend.h"
#include <QFile>

IvcBackend::IvcBackend(const QString &un_path) :
    mManager(mXs),
    mLog("IvcBackend"),
    mUnPath(un_path)
{
    mLog.setStreamBuffer(&gSyslog);
    qRegisterMetaType<domid_t>("domid_t");

    // New connections handler.
    QObject::connect(&mProcessServer, &QLocalServer::newConnection,
            this, &IvcBackend::addProcess,
            Qt::QueuedConnection);

    // Add/Remove guest controller handlers from GuestManager.
    QObject::connect(&mManager, &GuestManager::addGuest,
            this, &IvcBackend::onAddGuest,
            Qt::QueuedConnection);
    QObject::connect(&mManager, &GuestManager::removeGuest,
            this, &IvcBackend::onRemoveGuest,
            Qt::QueuedConnection);

    LOG(mLog, INFO) << "New backend: " << mUnPath.toStdString();
    QFile::remove(mUnPath);
    mProcessServer.listen(mUnPath);
}

IvcBackend::~IvcBackend()
{
    for (auto s : mServers) {
        delete s;
    }

    for (auto gc : mGuestControllers) {
        delete gc;
    }

    QFile::remove(mUnPath);
}

void
IvcBackend::onAddGuest(domid_t domid)
{
    // TODO: Fetch local domain-id. For now dom0 is where ivcdaemon exclusively
    // lives. Local IVC connections could be handled there.
    if (domid == 0)
        return;

    if (mGuestControllers.contains(domid)) {
        LOG(mLog, WARNING) << "dom" << domid << " controller already exists.";
        return;
    }

    GuestController* controller = new GuestController(mXs, domid);

    mGuestControllers.insert(domid, controller);
    for (auto server : mServers) {
        QObject::connect(controller, &GuestController::clientMessage,
                server, &IvcServer::onIngressMessage,
                Qt::QueuedConnection);
    }
}

void
IvcBackend::onRemoveGuest(domid_t domid)
{
    if (!mGuestControllers.contains(domid)) {
        LOG(mLog, WARNING) << "dom" << domid << " removed more than once.";
        return;
    }

    // Flush connections maps.
    for (auto server : mServers) {
        server->flushDomain(domid);
    }

    GuestController* controller = mGuestControllers.take(domid);
    delete controller;
}

GuestController*
IvcBackend::getController(domid_t domid)
{
    if (mGuestControllers.contains(domid))
        return mGuestControllers.value(domid);
    return nullptr;
}

void
IvcBackend::addProcess()
{
    if (!mProcessServer.hasPendingConnections())
        return;

    QLocalSocket* socket = mProcessServer.nextPendingConnection();
    IvcServer* server;

    try {
        server = new IvcServer(*this, socket);
    } catch (const std::exception& e) {
        LOG(mLog, ERROR) << "Failed to accept new server: " << e.what();
        return;
    }
    QObject::connect(socket, &QLocalSocket::disconnected,
            [this, server, socket]() {
                this->removeProcess(server);
                /* delete sock;*/
            });
    mServers.append(server);

    for (auto &it : mGuestControllers) {
        QObject::connect(it, &GuestController::clientMessage,
                server, &IvcServer::onIngressMessage,
                Qt::QueuedConnection);
    }
}

void
IvcBackend::removeProcess(IvcServer* server)
{
    mServers.removeAll(server);
    delete server;
}

int
IvcBackend::sendControlMessage(const libivc_message_t& message)
{
    if (!mGuestControllers.contains(message.to_dom)) {
        LOG(mLog, ERROR) << "No controller for dom" << message.to_dom;
        return -ENOENT;
    }
    return mGuestControllers.value(message.to_dom)->sendControlMessage(message);
}


/*
 * Local variables:
 * mode: C++
 * c-file-style: "BSD"
 * c-basic-offset: 4
 * tab-width: 4
 * indent-tabs-mode: nil
 * End:
 */
