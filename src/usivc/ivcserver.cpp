#include "ivcbackend.h"
#include "ivcserver.h"

//
// IvcConnection
//
IvcConnection::IvcConnection(domid_t domid, domid_t peerDomid, uint16_t port,
        uint64_t id, uint32_t eventChannel)
    : mDomid(domid),
      mPeerDomid(peerDomid),
      mPort(port),
      mId(id),
      mEventChannel(eventChannel)
{}

IvcConnection::IvcConnection(const libivc_message_t& message)
    : IvcConnection(message.from_dom, message.to_dom, message.port,
            message.connection_id, message.event_channel)
{}

IvcConnection::~IvcConnection()
{}

bool
IvcConnection::operator<(const IvcConnection& other) const
{
    if (mDomid != other.getDomid())
        return mDomid < other.getDomid();

    if (mPeerDomid != other.getPeerDomid())
        return mPeerDomid < other.getPeerDomid();

    if (mPort != other.getPort())
        return mPort < other.getPort();

    return mId < other.getId();
}

std::ostream&
IvcConnection::print(std::ostream& os) const
{
    return os << "IVC connection: dom" << mDomid << "->dom" << mPeerDomid
        << ":" << mPort << "(id:" << mId << ", evtchn:" << mEventChannel
        << ")";
}

libivc_message_t
IvcConnection::makeControlMessage(MESSAGE_TYPE_T type, int16_t status) const
{
    libivc_message_t msg = {
        .msg_start = HEADER_START,
        .from_dom = mDomid,
        .to_dom = mPeerDomid,
        .port = mPort,
        .event_channel = mEventChannel,
        .type = type,
        .num_grants = 1, // TODO: handle CONNECT with more than 1 (ctrl-ring).
        .connection_id = mId,
        .descriptor = { 0 }, // TODO: handle CONNECT with grand-refs.
        .status = status,
        .msg_end = HEADER_END,
    };
    return msg;
}

//
// IvcServer
//
IvcServer::IvcServer(IvcBackend& be, QLocalSocket* sock)
    : mSock(sock),
      mBackend(be),
      mLog("IvcServer")
{
    mLog.setStreamBuffer(&gSyslog);

    if (mSock == nullptr) {
        throw IvcException("Server socket is not initialized.", -EINVAL);
    }

    QObject::connect(mSock, &QLocalSocket::readyRead, this,
            &IvcServer::onEgressMessage,
            Qt::QueuedConnection);
}

IvcServer::~IvcServer()
{
    // Close all tracked connections.
    this->disconnectAll();
}

void
IvcServer::disconnectAll()
{
    int rc;

    for (const auto &it : mConnections.keys()) {
        const IvcConnection& con = it.reverse();
        const libivc_message_t& msg = con.makeControlMessage(DISCONNECT, 0);

        LOG(mLog, DEBUG) << msg;
        rc = mBackend.sendControlMessage(msg);
        if (rc < 0) {
            LOG(mLog, WARNING) << "Failed to send: " << msg;
        }
    }
}

void
IvcServer::flushDomain(domid_t domid)
{
    QList<IvcConnection> matches;

    for (const auto &it : mConnections.keys()) {
        if (it.getDomid() == domid ||
                it.getPeerDomid() == domid)
            matches.append(it);
    }

    for (const auto &it : matches) {
        LOG(mLog, DEBUG) << "Flush: " << it;
        mConnections.remove(it);
    }
}

// IvcServers do not send CONNECT, they only receive.
int
IvcServer::processMessage(const libivc_message_t& msg) {
    IvcConnection con(msg);

    LOG(mLog, DEBUG) << msg;

    switch (msg.type) {
        case CONNECT:
            if (mConnections.contains(con)) {
                //LOG(mLog, ERROR) << "Dropped redundant message: " << msg;
                return -EINVAL;
            }
            mConnections.insert(con, CONNECT);
            break;
        case ACK:
            if (!mConnections.contains(con.reverse()) ||
                mConnections[con.reverse()] != CONNECT) {
                //LOG(mLog, ERROR) << "Dropped incoherent message: " << msg;
                return -EINVAL;
            }
            if (msg.status != SUCCESS)
                mConnections.remove(con.reverse());
            else
                mConnections[con.reverse()] = ACK;
            break;
        case DISCONNECT:
        case DOMAIN_DEAD:
            mConnections.remove(con);
            mConnections.remove(con.reverse());
            break;
        case NOTIFY_ON_DEATH:
            break;
        default:
            LOG(mLog, WARNING) << "Dropped unhandled control message: " << msg;
            return -EINVAL;
    }
    return 0;
}

void
IvcServer::onIngressMessage(const libivc_message_t& msg)
{
    int rc;
    qint64 n;

    rc = this->processMessage(msg);
    if (rc < 0)
        return;

    n = mSock->write((const char*)(&msg), sizeof(msg));
    if (n != sizeof(msg)) {
        LOG(mLog, ERROR) << "Failed to write to server:" << n;
        return;
    }
    mSock->flush();
}

void
IvcServer::onEgressMessage()
{
    libivc_message_t msg;
    qint64 msg_len = static_cast<qint64>(sizeof(msg));
    qint64 n;
    int rc;

    while (mSock->bytesAvailable() >= msg_len) {
        n = mSock->read((char*)&msg, msg_len);
        if (n != msg_len) {
            // Likely the connection over the UNIX socket went out.
            LOG(mLog, ERROR) << "Failed to read from client:" << n;
            return;
        }
        // TODO: Sanity check message before processing?

        rc = this->processMessage(msg);
        if (rc < 0)
            continue;

        rc = mBackend.sendControlMessage(msg);
        if (rc < 0)
            LOG(mLog, ERROR) << "Failed to send message: " << rc;
    }
}
